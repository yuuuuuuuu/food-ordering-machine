import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachineGUI {
    private JPanel rootegh;
    private JButton StrawberryFrappuccinoButton;
    private JButton EspressoFrappuccinoButton;
    private JButton CoffeeFrappuccinoButton;
    private JButton GreenTeaFrappuccinoButton;
    private JButton CaramelFrappuccinoButton;
    private JButton DarkMochaFrappuccinoButton;
    private JTextPane orderedItemsList;
    private JButton checkOutButton;
    private JTextPane Totalcost;
    private JTextPane orderedItemsCost;
    private JTextPane orderedItemsSize;
    private JButton productDetailsButton1;
    private JButton productDetailsButton2;
    private JButton productDetailsButton3;
    private JButton productDetailsButton4;
    private JButton productDetailsButton5;
    private JButton productDetailsButton6;

    int sum=0;

    public FoodOrderingMachineGUI() {
        Totalcost.setText("Total "+sum+"yen.");
        StrawberryFrappuccinoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Strawberry Frappuccino",660);
            }
        });
        StrawberryFrappuccinoButton.setIcon(new ImageIcon(this.getClass().getResource("ストロベリー.jpg")));

        EspressoFrappuccinoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Espresso Frappuccino",590);
            }
        });
        EspressoFrappuccinoButton.setIcon(new ImageIcon(this.getClass().getResource("エスプレッソ.jpg")));

        CoffeeFrappuccinoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Coffee Frappuccino",460);
            }
        });
        CoffeeFrappuccinoButton.setIcon(new ImageIcon(this.getClass().getResource("コーヒー.jpg")));

        GreenTeaFrappuccinoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Green Tea Frappuccino",525);
            }
        });
        GreenTeaFrappuccinoButton.setIcon(new ImageIcon(this.getClass().getResource("抹茶.jpg")));

        CaramelFrappuccinoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Caramel Frappuccino",510);
            }
        });
        CaramelFrappuccinoButton.setIcon(new ImageIcon(this.getClass().getResource("キャラメル.jpg")));

        DarkMochaFrappuccinoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Dark Mocha Frappuccino",535);
            }
        });
        DarkMochaFrappuccinoButton.setIcon(new ImageIcon(this.getClass().getResource("ダークモカ.jpg")));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(null,
                        "Would you like to CheckOut?",
                        "CheckOut Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    JOptionPane.showMessageDialog(null,
                            "Thank you for ordering Drink! The total cost is "+sum+"yen.");
                    orderedItemsList.setText("");
                    orderedItemsSize.setText("");
                    orderedItemsCost.setText("");
                    sum=0;
                    Totalcost.setText("Total "+sum+"yen.");
                }
            }
        });
        productDetailsButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        "A Frappuccino with a red velvet cake motif that colors the holiday season.");
            }
        });
        productDetailsButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        "Frappuccino for adults, not too sweet and bittersweet.");
            }
        });
        productDetailsButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        "Frappuccino that simply tastes the deliciousness of coffee.");
            }
        });
        productDetailsButton4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        "A coffee-based frappuccino with the bitterness of dark chocolate.");
            }
        });
        productDetailsButton5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        "Coffee frappuccino blended with caramel syrup and topped with whipped cream and caramel sauce.");
            }
        });
        productDetailsButton6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        "Cream-based frappuccino blended with richly flavored matcha powder.");
            }
        });
    }

    void order(String food,int cost){
        int i;
        int confirmation=JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        String sizeselect[]={"Small","Tall","Grande","Venti"};
        if(confirmation==0){
            int value=JOptionPane.showOptionDialog(null,"Please select a size below.","Size Selection",
                    JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,null,sizeselect,sizeselect[1]);
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering "+food+"! It will be served as soon as possible.");
            String currentText1=orderedItemsList.getText();
            orderedItemsList.setText(currentText1+food+"\n");
            for(i=0;i<4;i++) {
                if (value == i) {
                    String currentText2 = orderedItemsSize.getText();
                    orderedItemsSize.setText(currentText2 + sizeselect[i] + "\n");
                    cost += i*30;
                    String currentText3=orderedItemsCost.getText();
                    orderedItemsCost.setText(currentText3+cost+"yen.\n");
                }
            }
            sum+=cost;
            Totalcost.setText("Total "+sum+"yen.\n");
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachineGUI");
        frame.setContentPane(new FoodOrderingMachineGUI().rootegh);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
